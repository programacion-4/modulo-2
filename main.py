from sqlalchemy import create_engine,MetaData,Column, String,Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database
import getpass

Base = declarative_base()

class model(Base):
    __tablename__= "palabras"
    palabra = Column(String,primary_key=True)
    definicion = Column(String)
    def __init__(self,palabra,definicion):
        self.palabra = palabra
        self.definicion = definicion

def create(engine): 
    meta = MetaData()
    mydb = Table(
    'palabras',meta,
    Column('palabra',String(1000),primary_key=True),
    Column('definicion',String(1000)))
    meta.create_all(engine) 

class ORM():
    def __init__(self,nameTable,dialect): 
        self.nameTable = nameTable+'.db' 
        self.dialect = dialect+self.nameTable
        self.engine = create_engine(self.dialect)  
        if not database_exists(self.engine.url): 
            create_database(self.engine.url)
            create(self.engine)
        self.session = sessionmaker(bind = self.engine)() 

    def post(self,palabra,definicion):
        try:
            self.session.add(model(palabra,definicion))
            self.session.commit()
            print('La palabra se añadido exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def getDefinicion(self,palabra):
        try:
            resultado = self.session.query(model).filter(model.palabra == palabra).one()
            print('Palabra:'+resultado.palabra+' Definicion:'+resultado.definicion)
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def getAllPalabras(self):
        try:
            resultado = self.session.query(model).all()
            for informacion in resultado:
                print('Palabra:'+informacion.palabra+'\n')
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def updatePalabra(self,palabra,newPalabra):
        try:
            resultado = self.session.query(model).filter(model.palabra == palabra).one()
            resultado.palabra = newPalabra
            self.session.add(resultado)
            self.session.commit()
            print('La palabra se actualizado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def updateDefinicion(self,palabra,definicion):
        try:
            resultado = self.session.query(model).filter(model.palabra == palabra).one()
            resultado.definicion = definicion
            self.session.add(resultado)
            self.session.commit()
            print('La definicion se actualizado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)
    
    def delete(self,palabra):
        try:
            resultado = self.session.query(model).filter(model.palabra == palabra).one()
            self.session.delete(resultado)
            self.session.commit()
            print('La palabra se eliminado exitosamente')
        except Exception as err:
            print('algo salio mal:')
            print(err)

def main(nombreTabla,user,password,host):
    try:
        orm = ORM(nombreTabla,f'mysql+pymysql://{user}:{password}@{host}')
        exit = False
        op = input('1-Agregar palabra  \n2-Actualizar palabra  3-Actualizar definicion  \n4-Lista de palabras  5-Buscar difinicion  \n6-Eliminar palabra   7-SALIR 🏃‍♂️ \n')
        if(str(op) == '1'):
            print('⚠  RES:')
            orm.post(input('introduzca la palabra:'),input('introduzca su definicion:'))
            
        elif(str(op) == '2'):
            print('⚠  RES:')
            orm.updatePalabra(input('introduzca la palabra que quiera actualizar:'),input('introduzca la nueva palabra:'))
            
        elif(str(op) == '3'):
            print('⚠  RES:')
            orm.updateDefinicion(input('introduzca la palabra que quiera actualizar su definicion:'),input('introduzca la nueva definicion:'))
            
        elif(str(op) == '4'):
            print('⚠  RES:')
            orm.getAllPalabras()
            
        elif(str(op) == '5'):
            print('⚠  RES:')
            orm.getDefinicion(input('introduzca la palabra que quiera buscar la definicion:'))
            
        elif(str(op) == '6'):
            print('⚠  RES:hay veces en la que debe salir para que se apliquen los cambios')
            orm.delete(input('introduzca la palabra que quieras borrar:'))
        
        elif(str(op) == '7'):
            exit = True
        if(not exit):
            main(nombreTabla,user,password,host)
        print('⚠  RES:Saliendo')
    except Exception as err:
            print('algo salio mal:')
            print(err)
    


if __name__ == "__main__":
    print('\nBIENCENIDO AL DICCIONARIO DE SLANG PANAMEÑO ✨\n')
    print('nota:"host por defecto localhost/"')
    main(input('Itroduzca nombre de la tabla:'),input('Itroduzca nombre de usuario:'),getpass.getpass(),input('Itroduzca host:'))
